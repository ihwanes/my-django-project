from django import forms
class ScheduleForm(forms.Form):
  error_messages = {
    'required': 'Tolong isi input ini',
  }
  attrs = {
    'class': 'form-control'
  }
  name = forms.CharField(label='Nama Kegiatan', required=False, max_length=100, widget=forms.TextInput(attrs=attrs))
  place = forms.CharField(label='Tempat', required=False, max_length=100, widget=forms.TextInput(attrs=attrs))
  day = forms.CharField(label='Hari', required=False, max_length=100, widget=forms.TextInput(attrs=attrs))
  date = forms.DateField(label='Tanggal', required=False, widget=forms.TextInput(attrs=attrs))
  time = forms.CharField(label='Jam', required=False, max_length=100, widget=forms.TextInput(attrs=attrs))
  category = forms.CharField(label='Kategori', required=False, max_length=100, widget=forms.TextInput(attrs=attrs))
  