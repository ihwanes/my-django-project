from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import ScheduleForm
from .models import Message
response = {}
# Create your views here.
def index(request):
  response['message_form'] = ScheduleForm
  return render(request, 'djangoform.html', response)

def myPage(request):
  return render(request, 'ihwanes.html')

def djangoForm(request):
  # Message.objects.all().delete()
  if request.method == "POST":
    form = ScheduleForm(request.POST or None)
    if form.is_valid():
      form.save()
      return HttpResponseRedirect('udahkesubmit')
    
    return render(request, 'djangoform.html', {'form':form})    
  else:
      form = ScheduleForm()
  return render(request, 'djangoform.html', {'form':form})

def message_post(request):
  message = Message.objects.all()

  return render(request,'listjadwal.html',{'messages':message})