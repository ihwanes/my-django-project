from django.db import models
from django.utils import timezone


class Message(models.Model):
  name = models.CharField(max_length=200, null = True)
  place = models.CharField(max_length=200, null = True)
  day = models.CharField(max_length=200, null = True)
  date = models.DateField(null = True)
  time = models.CharField(max_length=200, null = True)
  category = models.CharField(max_length=200, null = True)

  def publish(self):
      self.published_date = timezone.now()
      self.save()

  def __str__(self):
      return self.name
